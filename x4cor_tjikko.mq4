///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// -------------------------- Seznam modulů ---------------------------------
/*
  
  1. NewBars ..... volá jednotlivé funkce ve správný čas
              .... hlídá obchodní hodiny pro jednotlivé symboly - pro každý extra

  2. History  .... hlídá uzavřené obchody, může být single nebo pro testování součást programu

  3. FPI      .... počítá FPI pro jednotlivé sestavy

  4. DIff     .... počitá diference pro jednotlivé sestavy - použité měny

*/
// -------------------------- Seznam modulů ---------------------------------
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// -------------------------- Prepinace ---------------------------------


  
// -------------------------- Constant ---------------------------------
  #property strict

  #include <stdlib.mqh>

  //------- compile conditions
  // ---- output --------
  #define _SHOW_          1       // pri zaremovani vypina vsechny vystupy - pro rychlost ladeni
  #define _LOG_           1       // zapíná výstup MQL4/log/...

  // --- type trade -------
  //#define  _REAL_       1       // pro enable real trade
  //#define   _TRADE_SELF_  1     // obchoduje, false - posílá pouze signály
  
  // ------ indicators

  // -------- single programy:
  //#define _ONLY_HISTORY   1       // single program - pouze pro hlídání historie
  //#define _ONLY_MM        1       // MM pro všechny otevřené obchody

  #property copyright   "Copyright 2015, xCor8 & Milan Hemzal"
  #property link        "MultiAvg@turingalgo.com"
  #property version     "1.003"


  // ----------- Not For History
  #ifdef _SHOW_
  #property description "version with grafic output"
  #endif

  #ifndef _SHOW_
  #property description "fast version - no grafic output to screen"
  #endif

  #ifdef _TRADE_SELF_
  #property description "Signal version - send siganl to GLOBAL VARIABLES"
  #endif


  #property description "Application is ready for real trading"


  // other 
  #define   _WRONG      -999
  #define   _LONG       OP_BUY
  #define   _SHORT      OP_SELL
  #define   _BOTH       999

  #define   _CROSSUP    10
  #define   _CROSSDN    20

  #define  _SET_HISTORY  "X4_HISTORY"
  #define   NAME_DESK  "Show_BASE"

  #define   EPSILON 0.00000001



// -------------------------- OpenTrade ---------------------------------
  
  extern  string  cHelpOrder="---- nastavit parametry pro obchody -------";
  double  factor;         // pro vypocty
  extern    int       magic=20131011;
  //extern  int     TotalRisk       = 10;     // maximální propad -DD

          double  dRiskValue      = 0;
  extern  int     StopLossPips    = 5;     // StopLoss in pip
          double  StopLoss;

  extern  int     TakeProfitPips  = 10;  // TakeProfit in pip
          double  TakeProfit;


  extern  bool      lProc=true;           // false - fixni velikost obchodu
  extern  double    nProc=0.00001;        // 1% z Equity == 0.0001

  extern  double    dMinLots=0.05;        // minimalni velikost obchodu
  extern  double    dMaxLots=0.1;         // maximalni velikost obchodu
          double    dMinLotsClose;

  extern  double    Risk_Of_Ruin=1.13;    // kolik procent se riskuje na jeden obchod
  extern  double    Brzda=5000.0;         // pokud klesnou finance pod tuto hranici, blokuje obchodování

  extern  double     MezZisku=50;         // na jeden LOT - přepočítá se na cenu za PIP
  // přepočítat podle Cena/PIP
  extern  int       nPokles=80;           // procenta - 1/2 close

  extern  int       nMaxOrders=5;         // maximální počet současně otevřených obchodů
  extern  int       nWait=5;              // min. pocet svicek mezi obchody


extern int nVzorec=1; // 1 = / /  2 = * / 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// -------------------------- Makra  ---------------------------------
// UP svíčka WHITEC(1)
// DN svíčka !WHITEC(1)

  #define  WHITEC(IDX) Open[IDX]<Close[IDX]
  #define  BLACKC(IDX) Open[IDX]>Close[IDX]

  #ifndef PTR
     #define PTR(object) GetPointer(object)
     #define PTR_DELETE(object) { if (CheckPointer(object)==POINTER_DYNAMIC) delete object; }
     #define PTR_INVALID(object) (object==NULL || CheckPointer(object)==POINTER_INVALID)
     #define PTR_VALID(object)  (CheckPointer(object)==POINTER_DYNAMIC) 
  #endif   

  
// -------------------------- Svičkové formace   ---------------------------------
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// if (profitClose < 1% balance) false
// if ( maxProfit > daily) -> activate triger
// if profitClose < 80% false
// -------------------------- Histroy ---------------------------------
// Init:
// InitHistory()

// output:
// lFreeHistory ... true -> uzavřené obchody neblokují obchodování
// All function for manipulate with history

  extern  int     nMaxDailyDD     = 1;      // maximální denní propad v %
  extern  int     nDailyProfit    = 5;      // denní zisk v %

  class _History{
    public:
      void    _History();
      void    Main();
      bool    GetStatus(){return lFreeHistory;}

    private:
      double  dCloseProfit;       // aktuálně v zavřených obchodech
      double  dPlan;              // plan zisku v absolut hodnotách
      double  dLost;              // možný propad

      int     nHistory;           // počet uzavřených obchodů v aktuální den
      int     nDay;

      bool    lFreeHistory;       // má se blokovat obchodování
      bool    lActivate;          // aktivuje se ochrana

      //---
      void    ReadHistory();
      void    Reset();
      void    ForDaily();
      void    SetHistory();
  };

  //---
  void _History::_History(){
    Reset();
  }
  //
  void _History::Reset(){
    nHistory    =_WRONG;        // oprvé musí otestovat
    lFreeHistory=false;     // blokovat obchodování
    dCloseProfit=0;
    dPlan       = 0;
    lActivate   = false;
    nDay        = TimeDay( TimeCurrent());
    ForDaily();
  }
  //---
  void _History::ReadHistory(){
    dCloseProfit=0;
    MqlDateTime str1; 
    TimeToStruct(TimeCurrent(),str1);
    str1.hour=0;
    str1.min=0;

    // nastavit půlnoc
    datetime tHistFrom=StructToTime( str1);   

    for(int i=0; i<OrdersHistoryTotal(); i++){
      if( !OrderSelect(i, SELECT_BY_POS, MODE_HISTORY) ) continue;  // select check
      if (OrderCloseTime() < tHistFrom) continue;                   // starší než dneska 

      if (OrderType() > OP_SELL) continue;                          // pouze market
      
      dCloseProfit +=OrderProfit()+OrderCommission()+OrderSwap();
    }
  }
  // --- spočítej plánovaný denní zisk
  void _History::ForDaily(){
    //  100% ....... Balance
    //    5% .......  x
    //  x=
    dPlan = AccountEquity()*nDailyProfit/100; // absolutní částka na denní zisk
    dLost = -1*AccountEquity()*nMaxDailyDD /100;  // absolutní částka na denní propad
    #ifdef _LOG_
    PrintTo(  StringFormat( "Nový den - plan DD (%.2f), plan zisku (%.2f)",dLost,dPlan ));
    #endif


  }
  // ---
  void _History::SetHistory(){
    if (lFreeHistory) GlobalVariableSet( _SET_HISTORY, 1 ); else GlobalVariableSet( _SET_HISTORY, 0 );    
  }
  // --- 
  void _History::Main(){
    // nový den
    if ( nDay != TimeDay( TimeCurrent())) Reset();
    
    if (nHistory == OrdersHistoryTotal()){
      // pouze při změně počtu orderů v history
      return;
    }
    nHistory = OrdersHistoryTotal();
    ReadHistory();    // načte dnešní balance

    lFreeHistory = true;  // init

    if (dCloseProfit < dLost){
      // pokud je menší než povolené - ukončí, jinak povolí obchodovaní
      lFreeHistory=false;
      #ifdef _LOG_
      PrintTo(  StringFormat( "DD Blok - dnešní ztráta (%.2f) je vyšší než povolená (%.2f)",dCloseProfit,dLost ));
      #endif
      
    }

    if (lActivate && dPlan > dCloseProfit){
      lFreeHistory = false; // při poklesu - zavřít
      #ifdef _LOG_
      PrintTo(  StringFormat( "ZISK Blok - dnešní zisk (%.2f) je vyšší než planovaný (%.2f)",dCloseProfit,dPlan ));
      #endif
    }
    
    if (dCloseProfit > dPlan){
      lActivate = true;
      dPlan = dCloseProfit;
    }

    SetHistory();
  }
  //----
  _History *cHistory;

  void InitHistory(){
    cHistory = new _History();
  }

  bool CallHistory(){
    cHistory.Main();
    return cHistory.GetStatus();
  }
  
// -------------------------- Histroy ---------------------------------
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// -------------------------- Call every TF ---------------------------------
// Init:
// InitNewBar()

// output:
// lOpenMarket ... true => smí se obchodovat, jsou obchodní hodiny, vstahuje se to i na pátek a neděli

// class a objekty pro obsluhu volání na začátku nové svíčky
  // ..-- použíté externí proměnné
  extern string cHelpBar     = " --- nastavení času ------";

  extern bool    lClosePatek = true;            // neotevírá v pátek po x hodine
  extern int nStopPatek      = 14;              // v kolik přestat v pátek obchodovat
  extern int nBeginPo        = 8;               // začátek obchodování v pondělí
  //extern bool lCloseStop     = false;           // mimo obchodní hodiny zavírá obchody
  extern bool lManageHour    = false;           // Vypne/zapne obchodní hodiny

  extern string cOpenHour    ="8,";             // Od které hodiny smí obchodovat
  extern string cCloseHour   ="16,";            // ukončení obchodovaní

  int aOpenHour[];
  int aCloseHour[];
  int nPocetHodin;
  bool lOpenMarket=false;                       // jsou povolené obchodní hodiny

  //------------ --------- NEW BAR INDICATOR ---------------------
  class NewBar{
    public:
      void      NewBar(int per);
      int       nPeriod;
      datetime  OldTime;
      bool      IsNewBar();
  };

  void NewBar::NewBar(int per){
    nPeriod = per;
    OldTime = iTime(_Symbol, nPeriod, 0);
  }


  bool NewBar::IsNewBar(){
    if (OldTime != iTime(_Symbol, nPeriod, 0) ){
      OldTime = iTime(_Symbol, nPeriod, 0);     
      return true;
    }
    return false;
  }

  // ------------------------------------------------------------
  class NewBarEx: public NewBar{
    public:
      void  NewBarEx(int per);
      bool  IsPatek(){return lPatek;} // true, pokud je aktivní pátek nebo neděle
      bool  IsOver(){return lOver;}
      bool  IsOpenMarket();

    private:
      bool      lPatek;
      bool      lOver;
  };

  void NewBarEx::NewBarEx(int per):NewBar(per){
    nPeriod = per;
    OldTime = iTime(_Symbol, nPeriod, 0);
    lPatek = false; // v pátek se smí otevírat
    lOver  = false; // zavřít vše    
  }

  bool NewBarEx::IsOpenMarket(){
    lOpenMarket=false;
    if (lManageHour){
      if (nPocetHodin > _WRONG){
        int nPom=Hour();
        for( int i = 0; i < nPocetHodin; i++ ){
          if (nPom > aOpenHour[i] && nPom<aCloseHour[i]) lOpenMarket=true; 
        }
      }
    }else{
      lOpenMarket=true; 
    } // pokud se mají ignorovat hodiny
    // kontrola pátku
    int n= DayOfWeek();
    int h=Hour();
    if (lClosePatek){
      lPatek = false;
      if ( (n > 4 || n == 0 )&& h > nStopPatek ) {lOpenMarket=false;lPatek = true;} // zavřeno pátek
      if (n == 1 && h < nBeginPo) lOpenMarket=false;                       // zavřeno v pondělí do 8 hodin
      
    }
    // zavřít vše bez pardonu
    if ( n > 4 && h > 20) {lOver = true; lOpenMarket=false;} else lOver=false;
    return lOpenMarket;    
  }


  NewBarEx  *cNewBarNormal;    // aktuální TF
  // sem přidávat případné další TF

  void InitNewBar(){
    cNewBarNormal= new NewBarEx(PERIOD_M1);         // pro aktuální TF

    // otevírací hodiny
    // inicializace povodlenzch hodin
    if (!lManageHour) return;
    int nPom=_WRONG;
    String2Array(aOpenHour,nPom,cOpenHour);
    String2Array(aCloseHour,nPocetHodin,cCloseHour);
    if (nPom != nPocetHodin){
      Alert("Nesedi pocet hodin OPEN/CLOSE");
      nPocetHodin=_WRONG;
    }    
  }

// -------------------------- Call every TF ---------------------------------
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// -------------------------- Memory - pro všechny stené ---------------------------------
// slouží pro uložení vypočetených hodnot
// 
// contruktor:
//  n .. počet záznamů v poli
//  c .. jméno pole
//  nT.. Jak vysoko - kde začít zobrazovat
//  add(double) ... nový záznam
//  
// crossOver
//  a)  
// crossBelow
//
//
//
  
  #define   MEMORYIND   10      // počet zaznamů v paměti

  class _memory{
    public:
      void _memory(int n, string c, string k,int nT);

      int       top;          // umístnění ukazatelů
      int       left;

      //------- output
      bool      crossUp(int nCross);  // přechod přes mez
      bool      crossDn(int nCross);
      bool      crossUp(double nCross,double nCross1);  // přechod přes mez
      bool      crossDn(double nCross,double nCross1);
      bool      rising(){return true;}  // pole roste
      bool      falling(){return true;}  // klesající pole

      int       GetStatus(){return aStatus[0];}
      double    GetLast(){return aData[0];}


      int       nPer;         // parametry pro WI, CCI a EMA, RSI
      int       nLenght;      // počet záznamů v paměti
      int       nPoint;       // pomocný ukazatel - aby nebylo potřebovat nLenght-2

      string    cName;        // název pole

      double    aData[];      // záznam s vypočtenými hodnotami     
      string    aName[];      // název prvků
      int       aStatus[];

      //---------- Input function
      void      add(double d);
      void      add(int n);
      void      setParam(int n){nPer=n;}
      void      ShowMemory();

  };
  //------ constructor
  void _memory::_memory(int n, string c,string k, int nT){
    left=btnLeftAxis;
    top=nT;
    cName=c;
    nLenght=n;
    nPoint=nLenght-2;


    ArrayResize( aData, n );
    ArrayInitialize( aData, 0 );
    ArrayResize( aName, n );
    ArrayResize( aStatus, n );
    
    #ifdef _SHOW_
      for( int i = 0; i < nLenght; i++ ){
        aName[i]=c+(string)i+k; 
        draw_label(110,clrBlack,top,left+i*10+100,aName[i]);
      }
      draw_signal(c,clrText,top,left,c+"_H"+k);
      // tento  vystup je nutné přepsat vlastním
      //draw_signal("Aktual:",clrText,top,left+220,c+"NO_TE");
      //draw_signal("Aktual:                   Avg:                +/-50",clrText,top,left+220,c+"NO_TE");

    #endif
  }
  //----

  void _memory::add(double d){
    
    for( int i = nPoint; i >= 0; i-- ){
      aData[i+1]=aData[i];      
      aStatus[i+1]=aStatus[i];      
    }
    aData[0]=d;
    aStatus[0]=_WRONG;
  }

  void _memory::add(int n){
    
    for( int i = nPoint; i >= 0; i-- ){
      aStatus[i+1]=aStatus[i];      
    }
    aStatus[0]=n;
  }

  bool _memory::crossUp(int nCross){
    if (aData[1]<nCross && aData[0] > nCross) return true;
    return false;
  }
  //-------
  bool _memory::crossDn(int nCross){
    if (aData[1] > nCross && aData[0] < nCross) return true;
    return false;
  }
  bool _memory::crossUp(double nCross,double nCross1){
    if (aData[1]<nCross && aData[0] > nCross1) return true;
    return false;
  }
  //-------
  bool _memory::crossDn(double nCross,double nCross1){
    if (aData[1] > nCross && aData[0] < nCross1) return true;
    return false;
  }
  //-----
  void  _memory::ShowMemory(){
    // zobrazí barvy
    #ifdef _SHOW_

    for( int i = 0; i < nLenght; i++ ){
      switch (aStatus[i]){
        case _LONG:
          //ObjectSet(aName[i],OBJPROP_COLOR,clrSeaGreen);
          ObjectSetText(aName[i],CharToStr( 110 ),12, "Wingdings", clrSeaGreen);
          break;
        case _SHORT:
          //ObjectSet(aName[i],OBJPROP_COLOR,clrFireBrick);
          ObjectSetText(aName[i],CharToStr( 110 ),12, "Wingdings", clrFireBrick);
          break;
        case _CROSSUP:
          //ObjectSet(aName[i],OBJPROP_COLOR,clrSeaGreen);
          ObjectSetText(aName[i],CharToStr( 233 ),12, "Wingdings", clrSeaGreen);
          break;
        case _CROSSDN:
          //ObjectSet(aName[i],OBJPROP_COLOR,clrFireBrick);
          ObjectSetText(aName[i],CharToStr( 234 ),12, "Wingdings", clrFireBrick);
          break;
        default:
          ObjectSetText(aName[i],CharToStr( 110 ),12, "Wingdings", clrText);
          // ObjectSet(aName[i],OBJPROP_COLOR,clrText);      

      };

    }

    #endif
  }
  
// -------------------------- Memory - pro všechny stené ---------------------------------
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// -------------------------- CLass DIFF ---------------------------------

  extern string defSymbols="EURCAD,USDCAD,EURUSD,AUDNZD,NZDUSD,AUDUSD,EURGBP,GBPUSD,EURJPY,USDJPY,GBPCAD,"; 

  class _cDiff {
    public:
      void    _cDiff();
      void    Main(int n=0);
      int     IsSignal(string cSym);

    private:
      //_memory   *aIndi[];   // pro zobrazení a uložení pomocných databáze
      _memory   *mSymbol[];       //  uloží trend
      int       aFactor[];
      string    aSymbol[];
      
      int       nCount;       // počet použitých symbolů
  };

  void _cDiff::_cDiff(){
    // vytvořit pole použítých symbolů
    String2Array(aSymbol,nCount,defSymbols);

    ArrayResize(aFactor,nCount+1);
    ArrayResize(mSymbol,nCount+1);

    for( int i = 0; i < nCount; i++ ){
      // multiple
      if (StringFind( aSymbol[i], "JPY" ) > 0)
        aFactor[i]=1;
      else
        aFactor[i]=100;      
      mSymbol[i]  =new _memory(MEMORYIND,aSymbol[i],(string)i,nTopIndi);
      nTopIndi+=20;             
    }
    for( int j = MEMORYIND-1; j > 0; j-- ){
      Main(j); // spočítat prošlé parametry
    }
    nTopIndi+=20;             

  }
  
  void _cDiff::Main(int n=0){
    double dPom1=0;

    for( int i = 0; i < nCount; i++ ){
      dPom1=(iOpen( aSymbol[i], PERIOD_M1, n )-iOpen( aSymbol[i], PERIOD_M1, n+1 ))*aFactor[i];

      mSymbol[i].add(dPom1);

      if (dPom1 < MEZ_MINUS) mSymbol[i].aStatus[0]=_LONG;
      else if (dPom1 > MEZ_PLUS) mSymbol[i].aStatus[0]=_SHORT;

      #ifdef _SHOW_
      #ifdef _LOG_
        string cPom=StringFormat(" S1: %s=%.5f",aSymbol[i],dPom1);
        if (dPom1 < 0)
          draw_text(cPom,clrFireBrick,mSymbol[i].top,mSymbol[i].left+220,aSymbol[i]+"_LOG"+(string)i);
        else
          draw_text(cPom,clrWheat,mSymbol[i].top,mSymbol[i].left+220,aSymbol[i]+"_LOG"+(string)i);

      #endif
      mSymbol[i].ShowMemory();
      #endif

    }
  }

  int _cDiff::IsSignal(string cSym){
    for( int i = 0; i < nCount; i++ ){
      if (aSymbol[i] == cSym){ 
        int nPom=mSymbol[i].GetStatus();
        if (nPom == _LONG) mSymbol[i].aStatus[0]= _CROSSUP;
        if (nPom == _SHORT) mSymbol[i].aStatus[0]= _CROSSDN;
        return nPom;
      }
    }
    return _WRONG;
  }

// -------------------------- CLass DIFF ---------------------------------
  _cDiff *cDiff;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// -------------------------- FPI ALL class ---------------------------------
  #define NROW 5
  #define NCOL 3
  #define CAL_DIV    1  //  a/b/c
  #define CAL_MUL    2  //  a*b/c 
  extern double MEZ_MINUS=-0.04;  // spodní MEZ - pro LONG
  extern double MEZ_PLUS  =0.04;  // horní MEZ - pro SHORT

  class _cFPI {
    public:
    void _cFPI();
    void Calculate();
    bool Main();

    string    aRing[NROW][NCOL];
    
    int       Multi[NROW];
    int       aType[NROW];
    bool      aSignal[NROW];
    double    aFpi[NROW]; 
    _memory   *mSymbol[NROW];       //  uloží trend

  };

  void _cFPI::_cFPI(){
    /*
    #data EURJPY - OK
    F1="EURJPY"
    F2="USDJPY"
    F3="EURUSD"
    MULTIPLE=100
    */
    int i=0;
    aRing[ i,0] = "EURJPY"; aRing[ i,1] = "USDJPY"; aRing[ i,2] = "EURUSD";
    Multi[i]=100;
    aType[i]=CAL_DIV;
    aSignal[i]=false;

    mSymbol[i]  =new _memory(MEMORYIND,aRing[ 0,0],"",nTopIndi);
    nTopIndi+=20;             

    /*
    #data AUDNZD - OK
    F1="AUDNZD"
    F2="NZDUSD"
    F3="AUDUSD"
    MULTIPLE=100
    */
    i++;
    aRing[ 1,0] = "AUDNZD"; aRing[ 1,1] = "NZDUSD"; aRing[ 1,2] = "AUDUSD";
    Multi[1]=100;
    aType[1]=CAL_MUL;
    aSignal[i]=false;

    mSymbol[1]  =new _memory(MEMORYIND,aRing[ 1,0],"",nTopIndi);
    nTopIndi+=20;             
    /*
    #data EURGBP - OK
    F1="EURGBP"
    F2="GBPUSD"
    F3="EURUSD"
    MULTIPLE=100
    */
    i++;
    aRing[ 2,0] = "EURGBP"; aRing[ 2,1] = "GBPUSD"; aRing[ 2,2] = "EURUSD";
    Multi[2]=100;
    aType[2]=CAL_MUL;
    aSignal[i]=false;

    mSymbol[2]  =new _memory(MEMORYIND,aRing[ 2,0],"",nTopIndi);
    nTopIndi+=20;             

    /*
    #data CAD - OK
    F1="GBPCAD"
    F2="USDCAD"
    F3="GBPUSD"
    MULTIPLE=1000
    */
    i++;
    aRing[ 3,0] = "GBPCAD"; aRing[ 3,1] = "USDCAD"; aRing[ 3,2] = "GBPUSD";
    Multi[3]=1000;
    aType[3]=CAL_DIV;
    aSignal[i]=false;

    mSymbol[3]  =new _memory(MEMORYIND,aRing[ 3,0],"",nTopIndi);
    nTopIndi+=20;             

    /*
    #data CAD - OK
    F1="EURCAD"
    F2="USDCAD"
    F3="EURUSD"
    MULTIPLE=1000
    */
    i++;
    aRing[ 4,0] = "EURCAD"; aRing[ 4,1] = "USDCAD"; aRing[ 4,2] = "EURUSD";
    Multi[4]=1000;
    aType[4]=CAL_DIV;
    aSignal[i]=false;

    mSymbol[4]  =new _memory(MEMORYIND,aRing[ 4,0],"",nTopIndi);
    nTopIndi+=20;             


  }

  void _cFPI::Calculate(){
    RefreshRates();
    double dPom=0;
    double d1,d2,d3;
    #ifdef _LOG_
    string cOutput;
    #endif

    for( int i = 0; i < NROW; i++ ){
      d1=MarketInfo(aRing[i][0], MODE_BID); // neobchoduje se?
      d2=MarketInfo(aRing[i][1], MODE_BID); // neobchoduje se?
      d3=MarketInfo(aRing[i][2], MODE_BID); // neobchoduje se?

      if (aType[i] == CAL_DIV){
        dPom=(1-(d1/d2/d3))*Multi[i];
        cOutput=StringFormat("%s / %s / %s = %1.5f",aRing[i][0],aRing[i][1],aRing[i][2],dPom);
      }else{
        dPom=(1-(d1*d2/d3))*Multi[i];
        cOutput=StringFormat("%s * %s / %s = %1.5f",aRing[i][0],aRing[i][1],aRing[i][2],dPom);
      }
      mSymbol[i].add(dPom); // uložit
      // aktuální FPI
      if (dPom > MEZ_PLUS){
        aSignal[i]=true;
        #ifdef _SHOW_
        mSymbol[i].aStatus[0]=_LONG;
        #endif
      }else if (dPom < MEZ_MINUS){
        aSignal[i]=true;
        #ifdef _SHOW_
        mSymbol[i].aStatus[0]=_SHORT;
        #endif
      }

      // minulý FPI
      dPom = mSymbol[i].GetLast();
      if (dPom > MEZ_PLUS){
        aSignal[i]=true;
      }else if (dPom < MEZ_MINUS){
        aSignal[i]=true;
      }

      #ifdef _SHOW_
      #ifdef _LOG_
        draw_text(cOutput,clrWheat,mSymbol[i].top,mSymbol[i].left+220,aRing[i][0]+"_LOG");
        if (aSignal[i])
          draw_text(cOutput,clrGold  ,mSymbol[i].top,mSymbol[i].left+220,aRing[i][0]+"_LOG"+(string)i);
        else
          draw_text(cOutput,clrWheat,mSymbol[i].top,mSymbol[i].left+220,aRing[i][0]+"_LOG"+(string)i);

      #endif
      // Cena je nad/pod EMA - pořet svíčet nad /pod - pro hlavní zvolený indikátor
      //if (aSignal[i])drawBig(236,clrSeaGreen,mSymbol[i].top,mSymbol[i].left+180,aRing[i][0]+"_SHOW");
      //else ObjectDelete( aRing[i][0]+"_SHOW" );

      mSymbol[i].ShowMemory();
      #endif

    }
  }

  bool _cFPI::Main(){
    Calculate();
    return true;
  }
  

  
// -------------------------- FPI ALL class ---------------------------------
  _cFPI *cFPI;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// -------------------------- Inicializace - včetně načtení parametrů ---------------------------------


  // ---- testuje, jestli se smí program spustit
  bool CanRUn(){
    // --- enable real trade -- only for know account number
    #ifdef _REAL_    
      string mchar[256];
      for (uint i = 0; i < 256; i++) mchar[i] = CharToStr((uchar)i);
      int    AllowedAccountNo = (int)(   mchar[49]+mchar[51]+mchar[48]+mchar[53]+mchar[48]+mchar[48]+mchar[52]+mchar[54]); 
        // 48 = 0, 49 = 1, 50 = 2, 51 = 3, 52 = 4, 53 = 5 57 = 9
        // 13050046
        //Print(AllowedAccountNo);
      
      if ( AccountNumber() != AllowedAccountNo ){
        Print("You don't have permission to use this script!");
        return false;
      }  
    #endif

    #ifndef _REAL_
      if (AccountInfoInteger(ACCOUNT_TRADE_MODE) == ACCOUNT_TRADE_MODE_REAL){
        Alert("This is only for demo account");
        return false;
      }
    #endif
    // otestovat STOP z GLOBAL
    if (GlobalVariableGet("TOTAL_STOP") == 1) return false;
    // smí obchodovat
    return true;
  }

  //------------------ Init for Indicator and grafic ------------------------

  bool InitIndi(){
    //cEma=new _emaMemory(MEMORYIND,"Pokus",10);
    nTopIndi=btnTopAxis;
    #ifdef _SHOW_
        RectLabelCreate(0,NAME_DESK,0,14,40,680,300,clrBlack);
    #endif    

    // redraw background

    cDiff=new _cDiff();
    cFPI =new _cFPI();
    cHistory= new  _History();


    #ifdef _SHOW_
      // nastavit velikost desky
      ObjectSetInteger(0,NAME_DESK,OBJPROP_YSIZE,nTopIndi+80); 
    #endif
    return true;
  }


  //------------------ System INIT ------------------------------------------
  int OnInit(){
    if (! CanRUn()) return (INIT_FAILED);

    InitIndi();
    InitNewBar();     // inicializace obsluhy časových volání

    Comment( "Magic: ",magic );
    return(INIT_SUCCEEDED);
  }

  void SetTPSL(string cS){
    factor          = PFactor(cS);
    #ifndef _TRADE_SELF_
    StopLoss        = StopLossPips/factor;
    TakeProfit      = TakeProfitPips/factor;
    #endif

  }
// -------------------------- Inicializace - včetně načtení parametrů ---------------------------------
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// --------------------------  Main - cyclyc run ---------------------------------

  //-----------------
  bool lGlobalEnable;
  bool CanOpen(){

    bool lRet = true;

    if (! CallHistory()){
      #ifdef _LOG_
      PrintTo( "Nelze otevřít ORDER - blokováno uzavřenými obchody" );
      #endif
      lRet = false;      
    }
      

    if (nCoutDown>0){
      #ifdef _LOG_
      PrintTo( "Nelze otevřít ORDER - neuplynula doba od posledního otevřeného" );
      #endif
      lRet = false;      
    }

    if (FreeMany()){
      #ifdef _LOG_
      PrintTo( "Nelze otevřít obchod - není volný kapitál" );
      #endif
      lRet = false;      
    }


    if (! lOpenMarket){
      #ifdef _LOG_
      PrintTo( "Nelze otevřít obchod - nejsou obchodní hodiny" );
      #endif
      lRet = false;      
    }


    if (!CanRUn()){
      #ifdef _LOG_
      PrintTo( "Nelze otevřít obchod - nemas opravnění" );
      #endif
      lRet = false;      
    }

    if (! GlobalVariableGet( "ENABLE") == 1 && !IsTesting()){ 
      lRet = false;      
      #ifdef _LOG_
      PrintTo( "Nelze otevřít order - není nastaveno GLOBAL ENABLE" );
      #endif
    } 

    if (nOpenOrder > nMaxOrders){
      lRet = false;      
      #ifdef _LOG_
      PrintTo( "NO order - prekrocen max pocet otevrenych" );
      #endif
    } 
      

    lGlobalEnable = lRet;

    return lRet;     
    
  }

  bool FreeMany(){
    // return True ---- blokuje obchodování
    double riskuj=0;
    

    //Print("Account balance = ",AccountBalance()); // zůstatek
    //Print("Account equity = ",AccountEquity());   // zůstatek - margin
    //Print("Account free margin = ",AccountFreeMargin());  //použitelný zůstatek

    double kapital=AccountBalance();
    double fMany= AccountFreeMargin();
    
    //Print("Kapitál: ",kapital);
    riskuj=kapital/fMany;
    
    
    // pokud je zůstatek menší tak se zakáže obchodování
    if (riskuj > Risk_Of_Ruin) return (true);  

    if (kapital < Brzda) return (true);

    //if (__DEBUG )  Print("FMany: ",fMany," kapitál: ", kapital, " riskuj: ",riskuj," Brzda: ",Brzda);

    return (false);
  }

  void DrawMain(){
    
    #ifdef _SHOW_

    int nLeftP=btnLeftAxis;
    int nTop=nTopIndi+20;

    ObjectDelete( "MARKET_Enable" );
    ObjectDelete( "GLOBAL_Enable" );
        

    if (lOpenMarket) drawBig(184,clrSeaGreen,nTop,nLeftP,"MARKET_Enable");

    nLeftP+=50;
    if (lGlobalEnable) drawBig(74,clrSeaGreen,nTop,nLeftP,"GLOBAL_Enable");

    #endif
    
  }


  //-----------------
  void OnTick(){

    int nOrder=_WRONG;
    // --- vypočty podle aktuálního TF
    if (cNewBarNormal.IsNewBar()){
      cNewBarNormal.IsOpenMarket();
      DrawMain();
      //ObjectDelete("warning");
      cDiff.Main();
      if (nCoutDown > 0) nCoutDown--;
    }
    cFPI.Main();
    for( int i = 0; i < NROW; i++ ){
      if (! cFPI.aSignal[i]) continue; // není signál
      for( int j = 0; j < NCOL; j++ ){
        // test jestli otevřít nějaký symbol
        nOrder=cDiff.IsSignal(cFPI.aRing[i][j]);
        if (nOrder > _WRONG){   
          if (CanOpen())
            OpenTrade(cFPI.aRing[i][j],nOrder);
        }
      }
      cFPI.aSignal[i]=false;
    }
    Monitor();
  }


// --------------------------  Main - cyclyc run ---------------------------------
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// -------------------------- Monitor ---------------------------------


  // ---
  double  dLong =0;
  double  dShort=0;
  extern int nWaitCandle=4;
  void PrintTo(string cPom){
    #ifdef _SHOW_
    draw_text(cPom,clrWheat,nTopIndi+75,btnLeftAxis,"warning");

    #endif
    Print( cPom );
  }


  void Monitor( ){
    double dProfit=0;

    int nTotal = OrdersTotal(); // počet otevřených obchodú - celkem
    nOpenOrder=0;               // reset - hlídá jen svoje

    for( int i = 0; i < nTotal; i++ ){
      if (! OrderSelect(i, SELECT_BY_POS,MODE_TRADES) ) continue;  // nepovedlo se otevřít
      if (OrderMagicNumber() != magic) continue;                   // není magic - přeskočit
      nOpenOrder++; 
      dProfit+=OrderProfit()-OrderCommission()+OrderSwap();
      int nCandle=(int)(TimeCurrent()-OrderOpenTime())/(_Period*60);
      if (nCandle > 4 ){ 
        bool lOk=OrderClose( OrderTicket(), OrderLots(), OrderClosePrice(),50);
        nOpenOrder--;
      }

    }
    #ifdef _SHOW_
      draw_text(StringFormat( "Orders: %d  Profit: %.2f ",nOpenOrder,dProfit),clrWheat,nTopIndi,btnLeftAxis,"profit_out");
    #endif

  }// monitor


    
// -------------------------- Monitor ---------------------------------
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// --------------------------  Exit System ---------------------------------

  void OnDeinit(const int reason ){
    GlobalVariableDel( strMagic );  // vymazat původní Magic ze seznamu
    ObjectsDeleteAll();             // delete all object on graph
    EventKillTimer();               // kill timer
    // TODO - předělat do pole ?? 
    PTR_DELETE (cNewBarNormal);
    PTR_DELETE(cDiff);

  }
  
// --------------------------  Exit System ---------------------------------
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// -------------------------- OpenTrade ---------------------------------
  


  double  dSpread;
  string  cOpenComment="";
  int     nOpenOrder=0;   
  int     nCoutDown=0;

  int OpenTrade(string cSymbolOrder, int smer ){
    int type;
    double price,take,stop,dLot;
    double dBid=MarketInfo(cSymbolOrder, MODE_BID); // neobchoduje se?
    double dAsk=MarketInfo(cSymbolOrder, MODE_ASK); // neobchoduje se?

    string name=TimeToStr( TimeCurrent());
    
    color tmpc;

    dLot = dMinLots;

    SetTPSL(cSymbolOrder); // nastvit TP a SL

    if (lProc){
      //Margin = 0.012 * (Capital + ProfitClosed);
      double dFin=AccountEquity()-AccountCredit(); // zisk
      dLot=dFin*(nProc/(nOpenOrder+1)); // původně místo dFin == AccountFreeMargin()
    }

    // hlídat nastavené meze
    if (dLot < dMinLots) dLot=dMinLots;
    if (dLot > dMaxLots) dLot=dMaxLots;

    dSpread=dAsk-dBid;


   
    if (smer == _LONG){

      //ScreenClose("**** ORDER BUY ****");
      price = NormalizeDouble(dAsk,(int)MarketInfo(cSymbolOrder, MODE_DIGITS));
      tmpc = clrGreen;
      take = CalculateTakeProfit(OP_BUY, price+dSpread,TakeProfit,cSymbolOrder );      
      stop = CalculateStopLoss(OP_BUY,price-dSpread,StopLoss,cSymbolOrder);
      type = OP_BUY;
      nCoutDown=nWait;
      int nTicketU = SendSingleTrade(type, cOpenComment, dLot, price, stop, take, magic,tmpc,cSymbolOrder);
      return nTicketU;
    }

    if (smer == _SHORT){  
      //ScreenClose("**** ORDER SELL ****");
      price = NormalizeDouble(dBid,(int)MarketInfo(cSymbolOrder, MODE_DIGITS));
      tmpc = clrRed;
      take = CalculateTakeProfit(OP_SELL, price-dSpread,TakeProfit,cSymbolOrder);
      stop = CalculateStopLoss(OP_SELL,price+dSpread,StopLoss,cSymbolOrder);
      type = OP_SELL;
      nCoutDown=nWait;
      int nTicketD = SendSingleTrade(type, cOpenComment, dLot, price, stop, take, magic,tmpc,cSymbolOrder);
      return nTicketD;
    }  
    return _WRONG;
  }


// -------------------------- OpenTrade ---------------------------------
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////
// 
// manipulate with orders

  //------
  double CalculateStopLoss(int type, double price,double _SLP,string cSym)
  {
     //Returns the stop loss for use in LookForTradingOpps and InsertMissingStopLoss
     double stop=0;
     //RefreshRates();
     
     if (type == OP_BUY || type == OP_BUYSTOP || type == OP_BUYLIMIT)
     {
        if (!CloseEnough(_SLP, 0) ) 
        {
           stop = price - _SLP;           
        }//if (!CloseEnough(_SLP, 0) )         
     }//if (type == OP_BUY)
     
     if (type == OP_SELL || type == OP_SELLSTOP || type == OP_SELLLIMIT)
     {
        if (!CloseEnough(_SLP, 0) ) 
        {
           stop = price + _SLP ;            
        }//if (!CloseEnough(StopLoss, 0) )         

     }//if (type == OP_SELL)
     stop=O_R_NormalizePrice(cSym,stop);
     return(stop);
     
  }//End double CalculateStopLoss(int type)

  double CalculateTakeProfit(int type, double price,double _TPROF,string cSym)
  {
     //Returns the stop loss for use in LookForTradingOpps and InsertMissingStopLoss
     double take=0;

     //RefreshRates();
     
     if (type == OP_BUY || type == OP_BUYSTOP || type == OP_BUYLIMIT)
     {
        if (!CloseEnough(_TPROF, 0) )
        {
           take = price + _TPROF ;
           //Print( "take : ", take );
           //HiddenTakeProfit = take;
        }//if (!CloseEnough(_TPROF, 0) )

                 
        //if (HiddenPips > 0 && take > 0) take = NormalizeDouble(take + (HiddenPips / factor), Digits);

     }//if (type == OP_BUY)
     
     if (type == OP_SELL || type == OP_SELLSTOP || type == OP_SELLLIMIT)
     {
        if (!CloseEnough(_TPROF, 0) )
        {
           take = price - _TPROF;
           //HiddenTakeProfit = take;         
        }//if (!CloseEnough(_TPROF, 0) )
        
        
        //if (HiddenPips > 0 && take > 0) take = NormalizeDouble(take - (HiddenPips / factor), Digits);

     }//if (type == OP_SELL)
     take=O_R_NormalizePrice(cSym,take);
     return(take);
     
  }//End double CalculateTakeProfit(int type)

  int SendSingleTrade(int type, string comment, double lotsize, double price, double stop, double take, int MagicNumber,color tmpc,string cSym)
  {
    //pah (Paul) contributed the code to get around the trade context busy error. Many thanks, Paul.
   
    int slippage = 50;
    int ticket=-1;
    if (Digits == 3 || Digits == 5) slippage = 100;
   
    int expiry = 0;

    //RetryCount is declared as 10 in the Trading variables section at the top of this file
    for (int cc = 0; cc < RetryCount; cc++)
    {
      for (int d = 0; (d < RetryCount) && IsTradeContextBusy(); d++) Sleep(100);
  
      ticket = OrderSend(cSym,type, lotsize, price, slippage, stop, take, comment, MagicNumber, expiry,tmpc);
      
      if (ticket > -1) break;//Exit the trade send loop
      if (cc == RetryCount - 1) return(_WRONG);
   
      //Error trapping for both
    }//for (int cc = 0; cc < RetryCount; cc++);
   
    if (ticket < 0)
    {
      string stype;
      if (type == OP_BUY) stype = "OP_BUY";
      if (type == OP_SELL) stype = "OP_SELL";
      if (type == OP_BUYLIMIT) stype = "OP_BUYLIMIT";
      if (type == OP_SELLLIMIT) stype = "OP_SELLLIMIT";
      if (type == OP_BUYSTOP) stype = "OP_BUYSTOP";
      if (type == OP_SELLSTOP) stype = "OP_SELLSTOP";
      int err=GetLastError();
      //Alert(Symbol(), " ", WindowExpertName(), " ", stype," order send failed with error(",err,"): ",ErrorDescription(err));
      //Print(Symbol(), " ", WindowExpertName(), " ", stype," order send failed with error(",err,"): ",ErrorDescription(err));
      Print("Err: ",cSym," ty: ",stype," lot: ",lotsize," price: ", price," stop: ",  stop," take: ", take,"-", comment);
      return(_WRONG);
    }//if (ticket < 0)  
   
    if (!CheckForMarket(ticket))
    {
      // sem se dostane, pokud vrátí číslo ticketu, ale nebude v seznamu obchodů
      Alert(cSym, " sent trade not in your trade history yet. Turn of this ea NOW.");
      //TODO: zablokovat na hodinu
      //TODO: obchod uzavřít
    }
    return(ticket);   
  }//End bool SendSingleTrade(int type, string comment, double lotsize, double price, double stop, double take)


  void ModifyOrder(int ticket, double stop, double take)
  {
     //Modifies an order already sent if the crim is ECN.

     if (stop == 0 && take == 0) return; //nothing to do

     if (!OrderSelect(ticket, SELECT_BY_TICKET) ) return;//Trade does not exist, so no mod needed
     
     //RetryCount is declared as 10 in the Trading variables section at the top of this file   
     for (int cc = 0; cc < RetryCount; cc++)
     {
        for (int d = 0; (d < RetryCount) && IsTradeContextBusy(); d++) Sleep(100);
          if (take > 0 && stop > 0)
          {
             while(IsTradeContextBusy()) Sleep(100);
             if (OrderModify(ticket, OrderOpenPrice(), stop, take, OrderExpiration(), CLR_NONE)) return;           
          }//if (take > 0 && stop > 0)
     
          else if (take != 0 && stop == 0)
          {
             while(IsTradeContextBusy()) Sleep(100);
             if (OrderModify(ticket, OrderOpenPrice(), OrderStopLoss(), take, OrderExpiration(), CLR_NONE)) return;
          }//if (take == 0 && stop != 0)

          else if (take == 0 && stop != 0)
          {
             while(IsTradeContextBusy()) Sleep(100);
             if (OrderModify(ticket, OrderOpenPrice(), stop, OrderTakeProfit(), OrderExpiration(), CLR_NONE)) return;
          }//if (take == 0 && stop != 0)
     }//for (int cc = 0; cc < RetryCount; cc++)
     
     //Got this far, so the order modify failed
     int err=GetLastError();
     Print(Symbol(), " SL/TP  order modify failed with error(",err,"): ",ErrorDescription(err));               
     Alert(Symbol(), " SL/TP  order modify failed with error(",err,"): ",ErrorDescription(err));               

  }//void ModifyOrder(int ticket, double tp, double sl)
  
// End Module: Orders
////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////
// Module: grafika

  // -------------------------------- Grafika ---------------------------------


  int       btnTopAxis                = 50;
  int       btnLeftAxis               = 30;
  int       nTopIndi;

  int       btnTopOrder                = 40;
  int       btnLeftOrder               = 244;
  int       nTopOrder;

  string    prefix="x4cor_tjikko"; 


  color   clrText=clrYellow;      // text output
  color   clrNeutral=clrYellow;   // neutral direction
  color   clrLong=clrSeaGreen;    // direction LONG
  color   clrShort=clrFireBrick;  // direction SHORT
  color   clrBack=clrBlack;         // background



  void drawBig(uchar arrow, color c, int top, int left,string label)//, string arrow, int x, int y)
  {
     
     
    ObjectDelete(label);
    ObjectCreate(label, OBJ_LABEL, 0, 0, 0);
    ObjectSetText(label,CharToStr(arrow),36, "Wingdings", c);
    ObjectSet(label, OBJPROP_CORNER, 0);         
    ObjectSet(label, OBJPROP_XDISTANCE, left);
    ObjectSet(label, OBJPROP_YDISTANCE, top);
  }

  void draw_signal(string arrow, color c, int top, int left,string label)//, string arrow, int x, int y)
  {
     
     
    ObjectDelete(label);
    ObjectCreate(label, OBJ_LABEL, 0, 0, 0);
    ObjectSetText(label,arrow,12,"courrier", c);
    ObjectSet(label, OBJPROP_CORNER, 0);       
    ObjectSet(label, OBJPROP_XDISTANCE, left);
    ObjectSet(label, OBJPROP_YDISTANCE, top);
  }

  void draw_label(uchar arrow, color c, int top, int left,string label)//, string arrow, int x, int y)
  {
     
     
    ObjectDelete(label);
    ObjectCreate(label, OBJ_LABEL, 0, 0, 0);
    ObjectSetText(label,CharToStr(arrow),12, "Wingdings", c);
    ObjectSet(label, OBJPROP_CORNER, 0);
     
    ObjectSet(label, OBJPROP_XDISTANCE, left);
    ObjectSet(label, OBJPROP_YDISTANCE, top);
  }

  void ShowBuy(int top,int left,string id){
    draw_label(233,clrSeaGreen,top,left,id);
  }
  void ShowSell(int top,int left,string id){
    draw_label(234,clrFireBrick,top,left,id);
  }
  void ShowCloseBuy(int top,int left,string id){
    draw_label(217,clrSeaGreen,top,left,id);
  }
  void ShowCloseSell(int top,int left,string id){
    draw_label(218,clrFireBrick,top,left,id);
  }

  void ShowSignalBuy(int top,int left,string id){
    draw_label(241,clrSeaGreen,top,left,id);
    ObjectSetText(id,CharToStr(241),18, "Wingdings", clrLong);
  }
  void ShowSignalSell(int top,int left,string id){
    draw_label(242,clrFireBrick,top,left,id);
    ObjectSetText(id,CharToStr(241),18, "Wingdings", clrShort);
  }

  void ShowClosePrepare(int top,int left,string id, int type){
    if (type==OP_BUY)
      draw_label(216,clrLong,top,left,id);
    else
      draw_label(216,clrShort,top,left,id);
  }

  void DeleteShow(string id){
    ObjectDelete( id );
  }
  //-----
  void ShowBrickL(string id,int top,int left){
    //string id=StringFormat("br_%d_%d",i,top);
    draw_label(110,clrGreen,top,left,id); 
  }
  void ShowBrickD(string id,int top,int left){
    //string id=StringFormat("br_%d_%d",i,top);
    draw_label(110,clrRed,top,left,id); 
  }

    //----  
  void draw_text(string s, color c, int y, int x, string label)//, string arrow, int x, int y)
    {
       ObjectCreate(label, OBJ_LABEL, 0, 0, 0);
       ObjectSetText(label,s,10, "courrier", c);
       ObjectSet(label, OBJPROP_XDISTANCE, x+50);
       ObjectSet(label, OBJPROP_YDISTANCE, y+3);
    }

  void draw_text_o(string s, color c, int y, int x, string label)//, string arrow, int x, int y)
    {
       ObjectCreate(label, OBJ_LABEL, 0, 0, 0);
       ObjectSet(label, OBJPROP_CORNER, CORNER_RIGHT_UPPER);         

       ObjectSetText(label,s,10, "courrier", c);
       ObjectSet(label, OBJPROP_XDISTANCE, x+50);
       ObjectSet(label, OBJPROP_YDISTANCE, y+3);
    }

  void DrawVlong(string ID, double Price = 0, color LineColor = clrSeaGreen, int LineWidth = 1, ENUM_LINE_STYLE LineStyle = STYLE_SOLID)
  {
    #ifndef _SHOW_
    return;
    #endif

    if (ObjectFind(0, ID) > -1){
      ObjectMove(ID, 0, Time[0], Price);
      return;  
    }
    ObjectCreate(ID, OBJ_HLINE, 0, Time[0], Price);
    ObjectSet(ID, OBJPROP_STYLE, LineStyle);
    ObjectSet(ID, OBJPROP_WIDTH, LineWidth);
    ObjectSet(ID, OBJPROP_COLOR, LineColor);

  }

  void DrawVshort(string ID, double Price = 0, color LineColor = clrFireBrick, int LineWidth = 1, ENUM_LINE_STYLE LineStyle = STYLE_SOLID)
  {
    #ifndef _SHOW_
    return;
    #endif
    
    if (ObjectFind(0, ID) > -1){
      ObjectMove(ID, 0, Time[0], Price);
      return;  
    }
       
    ObjectCreate(ID, OBJ_HLINE, 0, Time[0], Price);
    ObjectSet(ID, OBJPROP_STYLE, LineStyle);
    ObjectSet(ID, OBJPROP_WIDTH, LineWidth);
    ObjectSet(ID, OBJPROP_COLOR, LineColor);

  }

  // -- line
  void DrawLine(string ID, double Price = 0, color LineColor = clrGold, int LineWidth = 1, ENUM_LINE_STYLE LineStyle = STYLE_SOLID)
  {
    #ifndef _SHOW_
    return;
    #endif
     
    ObjectCreate(ID+"H", OBJ_HLINE, 0, Time[0], Price);
    ObjectSet(ID+"H", OBJPROP_STYLE, LineStyle);
    ObjectSet(ID+"H", OBJPROP_WIDTH, LineWidth);
    ObjectSet(ID+"H", OBJPROP_COLOR, LineColor);

    ObjectCreate(ID+"V", OBJ_VLINE, 0, Time[0], Price);
    ObjectSet(ID+"V", OBJPROP_STYLE, LineStyle);
    ObjectSet(ID+"V", OBJPROP_WIDTH, LineWidth);
    ObjectSet(ID+"V", OBJPROP_COLOR, LineColor);

  }

  //+------------------------------------------------------------------+ 
  //| Create rectangle label                                           | 
  //+------------------------------------------------------------------+ 
  bool RectLabelCreate(const long             chart_ID=0,               // chart's ID 
                     const string           name="RectLabel",         // label name 
                     const int              sub_window=0,             // subwindow index 
                     const int              x=0,                      // X coordinate 
                     const int              y=0,                      // Y coordinate 
                     const int              width=50,                 // width 
                     const int              height=18,                // height 
                     const color            back_clr=C'236,233,216',  // background color 
                     const ENUM_BORDER_TYPE border=BORDER_SUNKEN,     // border type 
                     const ENUM_BASE_CORNER corner=CORNER_LEFT_UPPER, // chart corner for anchoring 
                     const color            clr=clrRed,               // flat border color (Flat) 
                     const ENUM_LINE_STYLE  style=STYLE_SOLID,        // flat border style 
                     const int              line_width=1,             // flat border width 
                     const bool             back=false,               // in the background 
                     const bool             selection=false,          // highlight to move 
                     const bool             hidden=true,              // hidden in the object list 
                     const long             z_order=0)                // priority for mouse click 
    { 
    //--- reset the error value 
    ResetLastError(); 
    //--- create a rectangle label 
    if(!ObjectCreate(chart_ID,name,OBJ_RECTANGLE_LABEL,sub_window,0,0)) 
     { 
      Print(__FUNCTION__, 
            ": failed to create a rectangle label! Error code = ",GetLastError()); 
      return(false); 
    } 
    //--- set label coordinates 
    ObjectSetInteger(chart_ID,name,OBJPROP_XDISTANCE,x); 
    ObjectSetInteger(chart_ID,name,OBJPROP_YDISTANCE,y); 
    //--- set label size 
    ObjectSetInteger(chart_ID,name,OBJPROP_XSIZE,width); 
    ObjectSetInteger(chart_ID,name,OBJPROP_YSIZE,height); 
    //--- set background color 
    ObjectSetInteger(chart_ID,name,OBJPROP_BGCOLOR,back_clr); 
    //--- set border type 
    ObjectSetInteger(chart_ID,name,OBJPROP_BORDER_TYPE,border); 
    //--- set the chart's corner, relative to which point coordinates are defined 
    ObjectSetInteger(chart_ID,name,OBJPROP_CORNER,corner); 
    //--- set flat border color (in Flat mode) 
    ObjectSetInteger(chart_ID,name,OBJPROP_COLOR,clr); 
    //--- set flat border line style 
    ObjectSetInteger(chart_ID,name,OBJPROP_STYLE,style); 
    //--- set flat border width 
    ObjectSetInteger(chart_ID,name,OBJPROP_WIDTH,line_width); 
    //--- display in the foreground (false) or background (true) 
    ObjectSetInteger(chart_ID,name,OBJPROP_BACK,back); 
    //--- enable (true) or disable (false) the mode of moving the label by mouse 
    ObjectSetInteger(chart_ID,name,OBJPROP_SELECTABLE,selection); 
    ObjectSetInteger(chart_ID,name,OBJPROP_SELECTED,selection); 
    //--- hide (true) or display (false) graphical object name in the object list 
    ObjectSetInteger(chart_ID,name,OBJPROP_HIDDEN,hidden); 
    //--- set the priority for receiving the event of a mouse click in the chart 
    ObjectSetInteger(chart_ID,name,OBJPROP_ZORDER,z_order); 
    //--- successful execution 
    return(true); 
  } 

// End Module: grafic
////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////
// Module Magic
  string strMagic;
  bool MainMagic(string pre,int ma=0){
    
    // Ruční zadání má přednost
    if (ma == 0 ){
        magic=CreateMagicNumber(pre);
        strMagic=StringFormat("M_%d_%s",magic,_Symbol);
        while( GlobalVariableCheck( strMagic ) ) {
          magic++;
          strMagic=StringFormat("M_%d_%s",magic,_Symbol);
        }
    }else {
      magic=ma;// Magic Number
      strMagic=StringFormat("M_%d_%s",magic,_Symbol);
      if (GlobalVariableCheck( strMagic )) return false;
    }    
    // otestovat, jestli magic je v GLOBAL, Pokud ne, pridat, pokud je, tak increment
    return true;
  }

  //---
  int CreateMagicNumber(string pre) {
     int i, k;
     int h = 0;
     string key=pre + Symbol() +(string) Period();//+prefix;
     if (IsTesting()) {
        key = "_" + Symbol();
     }

     for (i = 0; i < StringLen(key); i++) {
        k = StringGetChar(key, i);
        h = h + k;
        //Print(h);
        h = bitRotate(h, 5); // rotate 5 bits
     }
    

     for (i = 0; i < StringLen(key); i++) {
        k = StringGetChar(key, i);
        h = h + k;
        // rotate depending on character value
        h = bitRotate(h, k & 0x0000000F);
     }

     // now we go backwards in our string
     for (i = StringLen(key); i > 0; i--) {
        k = StringGetChar(key, i - 1);
        h = h + k;
        // rotate depending on the last 4 bits of h
        h = bitRotate(h, h & 0x0000000F);
     }

     return(h & 0x7fffffff);
  }
  //---
  /**
  * Rotate a 32 bit integer value bit-wise
  * the specified number of bits to the right.
  * This function is needed for calculations
  * in the hash function makeMacicNumber()
  */
  int bitRotate(int value, int count) {
     int tmp, mask;
     mask = (0x00000001 << count) - 1;
     tmp = value & mask;
     value = value >> count;
     value = value | (tmp << (32 - count));
     return(value);
  }  


// End Module: Magic
////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////
// Module Utils

  string StringSubstrOld(string x,int a,int b=-1) {
    if (a < 0) a= 0; // Stop odd behaviour
    if (b<=0) b = -1; // new MQL4 EOL flag
    return StringSubstr(x,a,b);
  }

  //----
  bool String2Array(int &aH[],int &n, string cPom){
    // empty Array
    ArrayResize(aH,0);

    if ( cPom == "" ){
      return(false) ; // empty array
    }

    // find delimiter
    int i = StringFind( cPom, "," );

    while (i != -1) 
    {

      // Get part to process
      string part = StringSubstrOld( cPom, 0, i );

      // Add to array
      n=ArraySize( aH );
      ArrayResize( aH, n+1 );
      aH[n] =(int)part;

      // Trim input string
      cPom = StringSubstrOld( cPom, i + 1 );
      i = StringFind( cPom, "," );

    }//while (i != -1) 
    // pokud je show - přidat zobrazení
    // optimalizace
    
    n=ArraySize( aH );
    //Print( "Size: ",ArraySize( aH ) );
    return ( true );
  }

  bool String2Array(string &aH[],int &n, string cPom){
    // empty Array
    ArrayResize(aH,0);

    if ( cPom == "" ){
      return(false) ; // empty array
    }

    // find delimiter
    int i = StringFind( cPom, "," );

    while (i != -1) 
    {

      // Get part to process
      string part = StringSubstrOld( cPom, 0, i );

      // Add to array
      n=ArraySize( aH );
      ArrayResize( aH, n+1 );
      aH[n] = part;

      // Trim input string
      cPom = StringSubstrOld( cPom, i + 1 );
      i = StringFind( cPom, "," );

    }//while (i != -1) 
    // pokud je show - přidat zobrazení
    // optimalizace
    
    n=ArraySize( aH );
    //Print( "Size: ",ArraySize( aH ) );
    return ( true );
  }

  int            O_R_Setting_max_retries  = 10;
  double         O_R_Setting_sleep_time     = 4.0; /* seconds */
  double         O_R_Setting_sleep_max    = 15.0; /* seconds */
  int            RetryCount = 10;//Will make this number of attempts to get around the trade context busy error.

  double PFactor(string pair)
  {
    //This code supplied by Lifesys. Many thanks Paul - we all owe you. Gary was trying to make me see this, but I
    //coould not understand his explanation. Paul used Janet and John words
   
    int n=(int)MarketInfo( pair, MODE_DIGITS );  
    double PipFactor=MathPow(10,n-1);
    /*10000; // correct factor for most pairs

    if (StringFind(pair,"JPY",0) != -1 || StringFind(pair,"XAG",0) != -1)
    PipFactor = 100; // if jpy or silver

    if (StringFind(pair,"XAU",0) != -1)
    PipFactor = 10; // if gold

    if (StringFind(pair,"XAU",0) != -1)
    PipFactor = 10; // if gold
    */
    return (PipFactor);
  }//End double PFactor(string pair)

  //---
  double O_R_NormalizePrice(string symbol, double price)
  {
     double ts = MarketInfo(symbol, MODE_TICKSIZE);
     return(MathRound(price/ts) * ts);
  }

  //---
  bool CloseEnough(double num1, double num2){
     /*
     This function addresses the problem of the way in which mql4 compares doubles. It often messes up the 8th
     decimal point.
     For example, if A = 1.5 and B = 1.5, then these numbers are clearly equal. Unseen by the coder, mql4 may
     actually be giving B the value of 1.50000001, and so the variable are not equal, even though they are.
     This nice little quirk explains some of the problems I have endured in the past when comparing doubles. This
     is common to a lot of program languages, so watch out for it if you program elsewhere.
     Gary (garyfritz) offered this solution, so our thanks to him.
     */
     
     if (num1 == 0 && num2 == 0) return(true); //0==0
     if (MathAbs(num1 - num2) / (MathAbs(num1) + MathAbs(num2)) < EPSILON) return(true);
     
     //Doubles are unequal
     return(false);

  }//End bool CloseEnough(double num1, double num2)

  bool CheckForMarket(int ticket){
    //My thanks to Matt for this code. He also has the undying gratitude of all users of my trading robots
   
    int lastTicket = OrderTicket();

    int cnt = 0;
    int err = GetLastError(); // so we clear the global variable.
    err = 0;

    while (true) {
      /* loop through open trades */
      int total=OrdersTotal();
      for(int c = 0; c < total; c++) {
         if(OrderSelect(c,SELECT_BY_POS,MODE_TRADES) == true) {
            if (OrderTicket() == ticket) {
              // Select back the prior ticket num in case caller was using it.
              return (true);
            }
         }
      }

      cnt = cnt+1;
      if (cnt > O_R_Setting_max_retries) {
        return(false);
      }
      // try again
      Print("Did not find #"+(string)ticket+" in market, sleeping, then doing retry #"+(string)cnt);
      O_R_Sleep(O_R_Setting_sleep_time, O_R_Setting_sleep_max);
    }

    Print("Never found #"+(string)ticket+" in history! crap!");
    return(false);
  }//End bool CheckForMarket(int ticket)

  //=============================================================================
  //                              O_R_Sleep()
  //
  //  This sleeps a random amount of time defined by an exponential
  //  probability distribution. The mean time, in Seconds is given
  //  in 'mean_time'.
  //  This returns immediately if we are backtesting
  //  and does not sleep.
  //
  //=============================================================================
  void O_R_Sleep(double mean_time, double max_time){
    if (IsTesting()) {
      return;   // return immediately if backtesting.
    }

    double p = (MathRand()+1) / 32768.0;
    double t = -MathLog(p)*mean_time;
    t = MathMin(t,max_time);
    int ms = (int)t*1000;
    if (ms < 10) {
      ms=10;
    }
    Sleep(ms);
  }//End void O_R_Sleep(double mean_time, double max_time)


// End Module: Utils
////////////////////////////////////////////////////////////////////////////////////////

